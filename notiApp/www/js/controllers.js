/*global angular */
/*global console, alert */

(function () {

    'use strict';

    var ctrls = angular.module('notiApp.controllers', ['ionic', 'notiApp.services']);

    ctrls.controller('myCtrl', ['$scope', '$rootScope', '$ionicPlatform', '$cordovaLocalNotification', '$ionicPopup',
        function ($scope, $rootScope, $ionicPlatform, $cordovaLocalNotification, $ionicPopup) {

            $scope.showPopup = function () {
                //alert('test');
                $ionicPopup.alert({
                    title: 'Alert',
                    template: 'haha'
                }).then(function (res) {
                    //                    alert(res);
                });
            };

            $ionicPlatform.ready(function () {

                $scope.scheduleSingleNotification = function () {
                    $cordovaLocalNotification.schedule({
                        id: 1,
                        title: 'Title here',
                        text: 'Text here',
                        data: {
                            customProperty: 'custom value'
                        }
                    }).then(function (result) {
                        // ...
                        console.log('single notify: result', result);
                    });
                };

            });
        }]);

    ctrls.controller('trackListCtrl', ['$scope', '$http', '$ionicPopup', 'MediaPlayerService', function ($scope, $http, $ionicPopup, MediaPlayerService) {
        var apiUrl = 'http://subak.us.to:8081/api/sorinara/chart/top/weekly';

        //        $scope.isPlaying = false;
        //$scope.mediaStatus = Media.MEDIA_NONE;

        $scope.doRefresh = function () {
            $scope.loadTracks();
        };

        $scope.tracks = [];

        $scope.loadTracks = function () {
            //            apiUrl = 'http://localhost:8081/api/sorinara/chart/top/weekly';
            //            apiUrl = 'http://subak.us.to:8081/api/sorinara/chart/top/weekly';

            console.log('load tracks: url=', apiUrl);

            $http.get(apiUrl).success(function (data) {
                console.log('data=', data);
                $scope.tracks = data.tracks;
            }).error(function (data, status) {
                console.error('error in loading track. err=', data, status);
                $ionicPopup.alert({
                    title: 'Error',
                    template: 'Failed to load tracks. msg=[' + data + ']'
                });
            })['finally'](function () { // to avoid jslint for reserved word
                // Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');
            });
        };

        $scope.search = function () {
            console.log('search: keyword=', $scope.keyword);
            apiUrl = 'http://subak.us.to:8081/api/mp3fox/search/' + $scope.keyword;
            $scope.loadTracks();
        };

        $scope.player = MediaPlayerService;

        //        $scope.play = function (track) {
        //            MediaPlayerService.play(track);
        //        };

    }]);

    ctrls.controller('mediaPlayerCtrl', ['$scope', 'MediaPlayerService', function ($scope, MediaPlayerService) {

        $scope.player = MediaPlayerService;

        MediaPlayerService.on({
            name: 'player',
            callback: function (event) {
                console.log('mediaPlayerCtrl: event=', event);
            }
        });

    }]);

}());