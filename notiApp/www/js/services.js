/*global angular */
/*global console */

(function () {

    'use strict';

    var services = angular.module('notiApp.services', ['ionic', 'ngCordova', 'ngCordova.plugins']);

    services.service('MediaPlayerService', ['$cordovaMedia', function ($cordovaMedia) {
        var eventListeners = {},
            listenerId = 0,
            media,
            currState;

        this.States = {
            IDLE: {
                code: 0,
                text: 'Idle'
            },
            PLAYING: {
                code: 1,
                text: 'Playing'
            },
            PAUSED: {
                code: 2,
                text: 'Paused'
            }
        };

        this.Events = {
            STATE_CHANGED: {
                code: 0,
                text: 'StateChanged'
            }
        };

        currState = this.States.IDLE;

        function notifyEvent(event) {
            var key;

            for (key in eventListeners) {
                if (eventListeners.hasOwnProperty(key)) {
                    eventListeners[key].callback(event);
                }
            }
        }

        this.state = function (newState) {
            if (newState === undefined) {
                return currState;
            }
            console.log('state: newState=', newState);
            //            if (this.States.hasOwnProperty[newState] === false) {
            //                console.error('Unknown state: newState=', newState);
            //                return;
            //            }
            currState = newState;
            notifyEvent({
                event: this.Events.STATE_CHANGED,
                data: currState
            });
        };

        this.resume = function () {
            if (media !== undefined) {
                media.play();
                this.state(this.States.PLAYING);
            }
        };

        this.pause = function () {
            if (media !== undefined) {
                media.pause();
                this.state(this.States.PAUSED);
            }
        };

        this.play = function (track) {
            console.log('play: track=', track);
            console.log('play: state=', this.state(), '!idle=', this.state() !== this.States.IDLE);
            if (this.state() !== this.States.IDLE) {
                media.stop();
                media.release();
                media = undefined;
                this.state(this.States.IDLE);
            }

            //            media = new Media(track.file, function () {
            //                // success
            //            }, function (err) {
            //                // error
            //                console.log('error=', err);
            //            }, function (status) {
            //                // notify
            //                console.log('media status=', status);
            //            });

            this.track = track;
            media = $cordovaMedia.newMedia(track.file);

            media.play();
            this.state(this.States.PLAYING);
        };

        this.on = function (listener) {
            if (listener.name === undefined) {
                console.error('listener should have name. listener=', listener);
                return;
            }
            if (listener.callback === undefined) {
                console.error('listener should have callback. listener=', listener);
                return;
            }
            eventListeners[listener.name] = listener;
        };

        this.off = function (listener) {
            if (listener.name === undefined) {
                console.error('listener should have name. listener=', listener);
                return;
            }
            if (listener.callback === undefined) {
                console.error('listener should have callback. listener=', listener);
                return;
            }
            eventListeners[listener.name] = undefined;
        };

    }]);

}());