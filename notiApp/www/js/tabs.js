/*global angular */
/*global console */

(function () {

    'use strict';

    var tabs = angular.module('notiApp.tabs', ['ionic']);

    tabs.config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('tabs', {
                url: "/tab",
                abstract: true,
                templateUrl: "templates/tabs.html"
            })
            .state('tabs.player', {
                url: "/player",
                views: {
                    'player-tab': {
                        templateUrl: "templates/player.html"
                            //                        controller: 'HomeTabCtrl'
                    }
                }
            })
            .state('tabs.charts', {
                url: "/charts",
                views: {
                    'charts-tab': {
                        templateUrl: "templates/charts.html"
                            //                        controller: 'HomeTabCtrl'
                    }
                }
            })
            //            .state('tabs.facts', {
            //                url: "/facts",
            //                views: {
            //                    'home-tab': {
            //                        templateUrl: "templates/facts.html"
            //                    }
            //                }
            //            })
            //            .state('tabs.facts2', {
            //                url: "/facts2",
            //                views: {
            //                    'home-tab': {
            //                        templateUrl: "templates/facts2.html"
            //                    }
            //                }
            //            })
            .state('tabs.about', {
                url: "/about",
                views: {
                    'about-tab': {
                        templateUrl: "templates/about.html"
                    }
                }
            })
            //            .state('tabs.navstack', {
            //                url: "/navstack",
            //                views: {
            //                    'about-tab': {
            //                        templateUrl: "templates/nav-stack.html"
            //                    }
            //                }
            //            })
            .state('tabs.search', {
                url: "/search",
                views: {
                    'search-tab': {
                        templateUrl: "templates/search.html"
                    }
                }
            });


        $urlRouterProvider.otherwise("/tab/charts");

    });

    tabs.controller('HomeTabCtrl', function ($scope) {
        console.log('HomeTabCtrl');
    });

}());