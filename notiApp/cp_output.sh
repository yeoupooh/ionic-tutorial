#!/bin/bash

APP_PATH=$HOME/git/ionic-tutorial/notiApp
APK_FILE=$APP_PATH/platforms/android/build/outputs/apk/android-debug.apk

DEST_FILE=notiapp.apk
DROPBOX_PATH=$HOME/Dropbox/Projects/TheCoolMobile/beta

CMD="cp -v $APK_FILE $DROPBOX_PATH/$DEST_FILE"
#echo $CMD
$CMD
